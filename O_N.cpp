#include<bits/stdc++.h>
using namespace std;

int main()
{
    int N;
    cin >> N;
    int arr[N];

    for(int i=0; i<N; i++)  //O(N)
    {
        cin >> arr[i];
    }

    int s=0;
    for(int i=0;i<N; i+=2) //O(N)
    {
        s+=arr[i];
    }

    cout << s << endl;  //total time complexity is O(N)


    return 0;
}